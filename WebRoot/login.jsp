<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="renderer" content="webkit|ie-comp|ie-stand">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
		<!--[if lt IE 9]>
			<script type="text/javascript" src="lib/html5shiv.js"></script>
			<script type="text/javascript" src="lib/respond.min.js"></script>
		<![endif]-->
		<link href="static/h-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
		<link href="static/h-ui.admin/css/H-ui.login.css" rel="stylesheet" type="text/css" />
		<link href="static/h-ui.admin/css/style.css" rel="stylesheet" type="text/css" />
		<link href="lib/Hui-iconfont/1.0.8/iconfont.css" rel="stylesheet" type="text/css" />
		<!--[if IE 6]>
			<script type="text/javascript" src="lib/DD_belatedPNG_0.0.8a-min.js" ></script>
			<script>DD_belatedPNG.fix('*');</script>
		<![endif]-->
		<title>后台登录</title>
	</head>
	<body>
		<input type="hidden" id="TenantId" name="TenantId" value="" />
		<div class="header"></div>
		<div class="loginWraper">
			<div id="loginform" class="loginBox">
				${msg}
				<form class="form form-horizontal" action="login" method="post">
					<div class="row cl">
						<label class="form-label col-xs-3"><i class="Hui-iconfont">&#xe60d;</i></label>
						<div class="formControls col-xs-8">
							<input id="username" name="username" type="text" placeholder="账户" class="input-text size-L">
						</div>
					</div>
					<div class="row cl">
						<label class="form-label col-xs-3"><i class="Hui-iconfont">&#xe60e;</i></label>
						<div class="formControls col-xs-8">
							<input id="userpwd" name="userpwd" type="password" placeholder="密码" class="input-text size-L">
						</div>
					</div>
					<div class="row cl">
						<div class="formControls col-xs-8 col-xs-offset-3">
							<input class="input-text size-L" type="text" placeholder="验证码"
								onblur="if(this.value==''){this.value='验证码:'}"
								onclick="if(this.value=='验证码:'){this.value='';}" value="验证码:"
								style="width:150px;"> <img src=""> <a id="kanbuq"
								href="javascript:;">看不清，换一张</a>
						</div>
					</div>
					<div class="row cl">
						<div class="formControls col-xs-8 col-xs-offset-3">
								<!--取值的时候，考虑下，保持登录状态这个值是否有勾选，有勾选，把这个登录状态要保存下来了，下次在登录的时候，直接给你登录，直接进入主页---servlet里面  -->
							<label for="online"> <input type="checkbox" name="online"
								id="online" > 使我保持登录状态
							</label>
						</div>
					</div>
					<div class="row cl">
						<div class="formControls col-xs-8 col-xs-offset-3">
							<input name="" type="submit" class="btn btn-success radius size-L"
								value="&nbsp;登&nbsp;&nbsp;&nbsp;&nbsp;录&nbsp;"> <input
								name="" type="reset" class="btn btn-default radius size-L"
								value="&nbsp;取&nbsp;&nbsp;&nbsp;&nbsp;消&nbsp;">
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="footer">Copyright 你的公司名称 by H-ui.admin v3.1</div>
		<script type="text/javascript" src="lib/jquery/1.9.1/jquery.min.js"></script>
		<script type="text/javascript" src="static/h-ui/js/H-ui.min.js"></script>
	</body>
</html>
