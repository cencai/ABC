$(function() {
	/*// 初始化城市选择插件
	init_city_select($("#city"));*/
	
	/*$('.skin-minimal input').iCheck({
		checkboxClass : 'icheckbox-blue',
		radioClass : 'iradio-blue',
		increaseArea : '20%'
	});*/

	$("#form-consuming-add").validate({
		rules : {
			aName : {
				required : true,
				minlength : 2,
				maxlength : 16
			},
			
			beizhu : {
				minlength : 10,
				maxlength : 100
			}
		},
		onkeyup : false,
		focusCleanup : true,
		success : "valid",
		submitHandler : function(form) {
			// 异步提交
			$(form).ajaxSubmit({
				type : 'POST',
				url : 'OperateConsumingServlet',
				dataType : 'json',
				data : $(form).serialize(), // 将表单中的数据(有name属性的表单元素)序列化，转为json格式的参数
				success : function(data) {
					if(data.result) {
						layer.msg('修改成功！');
						setTimeout(function() {
							parent.layer.close(parent.layer.getFrameIndex(window.name));
						}, 800);
					} else {
						layer.msg('修改失败！');
					}
				},
				error : function(data) {
					console.log(data.msg);
				},
			});
		}
	});
	
	$("#beizhu").on('keyup', function() {
		if($(this).val().length > 100) {
			layer.msg('输入过长，最多为100个字符！');
			$(this).val($(this).val().substr(0, 100));
		}
		$(".textarea-length").html($(this).val().length);
	});
});