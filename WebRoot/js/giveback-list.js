var table;
$(function() {
	table = $('.table-sort').dataTable({
		"aaSorting" : [ [ 1, "desc" ] ],// 默认第几个排序
		"sAjaxSource" : "GetGiveBackServlet", // 请求资源的路径
		"bServerSide" : true, //是否启动服务器端数据导入
		"aLengthMenu" : [5, 10, 20], //更改显示记录数选项
		"iDisplayLength" : 5, //默认显示的记录数
		"iDisplayStart" : 0, //从第几条开始展示
		/*"bPaginate" : true,*/ //是否显示（应用）分页器，默认true
		// 自己封装提交参数
		"fnServerParams" : function(aoData) {
			aoData.push({
				"name" : "datemin",
				"value" : $("#datemin").val()
			},{
				"name" : "datemax",
				"value" : $("#datemax").val()
			},{
				"name" : "searchConditions",
				"value" : $("#searchConditions").val()
			});
		},
		// 用post提交
		"fnServerData" : function(sSource, aoData, fnCallback) {
			// jQuery的ajax提交
            $.ajax({  
            	"url" : sSource, /*提交路径，获取sAjaxSource设置的路径*/
            	"data" : aoData, /*提交参数*/
                "dataType" : 'json',  
                "type" : "POST",  
                /*"success" : fnCallback*/
                "success" : function(data) {
                	console.log(data);
                	fnCallback(data);
                },
                "error" : function(data) {
                	console.log(data.msg);
                }
            });  
        },  
		"aoColumns" : [
		   {
			   data : "gId",
			   orderable : false,
			   render : function(data, type, full, meta) {
				   return '<input type="checkbox" value="'+ data + '" class="delGId" name="delGId"/>';
			   }
		   },{
			   data : "gId"
		   },{
			   data : "appliance.aName"
		   },{
			   data : "appliance.amodel" 		//型号
		   },{
			   data : "appliance.standard"	//规格
		   },{
			   data : "count"
		   },{
			   data : "user.uname"//todo
		   },{
		  
			   data : "giveDate",
			   render : function(data, type, full, meta) {
				   return new Date(data).format("yyyy-MM-dd");
			   }
		   },{
			   data : "gStatus",
			   orderable : false,
			   render : function(data, type, full, meta) {
				   if(data == 0) {
					   return '<span class="label label-success radius">未启用</span>';
				   } else {
					   return '<span class="label label-success radius">已启用</span>';
				   }
			   }
		   },{
			   data	: "remark"	
		   },{
			   data : "gId",
			   orderable : false,
			   render : function(data, type, full, meta) {
				   return '<a title="编辑" href="javascript:;" onclick="giveback_edit(this)" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6df;</i></a>'
				   		 + '<a title="删除" href="javascript:;" onclick="giveback_del(' + data + ')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6e2;</i></a>';
			   }
		   }
		]
	});
	
	// 点击搜索按钮
	$("#searchBtn").on('click', function() {
		table.fnDraw();
	});
	
	// 点击批量删除按钮
	$("#delGiveBacksBtn").on('click', function() {
		layer.confirm('确认要删除吗？', function() {
			// 点击确认后，执行的删除操作
			$.ajax({
				type : 'post',
				dataType : 'json',
				url : 'OperateGiveBackServlet?operate=delete',
				data : $(".delGId:checked").serialize(), // 序列化
				success : function(data) {
					if(data.result) {
						layer.msg('删除成功！');
						table.fnClearTable(); // 重新加载数据
					} else {
						layer.msg('删除失败！');
					}
				},
                "error" : function(data) {
                	console.log(data.msg);
                }
			});
		});
	});
	// 点击添加按钮
	$("#addGiveBacksBtn").on('click', function() {
		layer.open({
			type: 2, // 2-iframe
			area: ['650px', '550px'],
			fix: false, //不固定
			maxmin: true,
			shade:0.4,
			title: '添加归还信息',
			content: 'giveback-add.jsp',
			end: function() {
				// 添加窗口销毁时，回调的函数 -- 刷新数据列表
				table.fnClearTable();
			}
		});
	});

});


/* 用户-编辑 */
function giveback_edit(obj) {
	// 获取操作的行
	var parent_trs = $(obj).parents("tr"); // 获取a标签所在的tr行
	// 获取行中有数据的元素
	var tds = parent_trs[0].cells;
	
	layer.open({
		type: 2, // 2-iframe
		area: ['700px', '600px'],
		fix: false, //不固定
		maxmin: true,
		shade:0.4,
		title: '编辑归还信息',
		content: 'giveback-modify.jsp',
		success: function(layero, index) {
			console.log(layero);
			console.log(index);
			// layero-当前层DOM, index-当前层索引
			// 成功打开窗口时的回调函数，完成填充数据的功能
			var body  = layer.getChildFrame('body', index);
			body.contents().find("#gId").val($(tds[1]).html());
			body.contents().find("#appName").val($(tds[2]).html());//名称，型号，规格
			body.contents().find("#amodel").val($(tds[3]).html());
			body.contents().find("#standard").val($(tds[4]).html());
			body.contents().find("#No").val($(tds[5]).html());
			body.contents().find("#username").val($(tds[6]).html());
			body.contents().find("#gDate").val($(tds[7]).html());
			body.contents().find("#beizhu").val($(tds[9]).html());
			
			
			/*// 设置单选框样式
			body.contents().find('.skin-minimal input').iCheck({
				radioClass : 'iradio-blue'
			});
			switch($(tds[3]).html()) {
			case '男' : 
				console.log(11);
//				body.contents().find("#sex-1").attr('checked', 'checked');
				body.contents().find("#sex-1").iCheck('check'); 
				break;
			case '女' : 
				console.log(22);
//				body.contents().find("#sex-2").attr('checked', 'checked');
				body.contents().find("#sex-2").iCheck('check'); 
				break;
			case '保密' : 
				console.log(33);
//				body.contents().find("#sex-3").attr('checked', 'checked');
				body.contents().find("#sex-3").iCheck('check'); 
				break;
			}*/
		},
		end: function() {
			// 添加窗口销毁时，回调的函数 -- 刷新数据列表
			table.fnClearTable();
		}
	});
}
/* 用户-删除 */
function giveback_del(id) {
	layer.confirm('确认要删除吗？', function(index) {
		// 点击确认后，执行的删除操作
		$.ajax({
			type : 'POST',
			url : 'OperateGiveBackServlet',
			dataType : 'json',
			data : {'operate' : 'delete', 'delGId' : id},
			success : function(data) {
				if(data.result) {
					layer.msg('删除成功！');
					table.fnClearTable(); // 重新加载数据
				} else {
					layer.msg('删除失败！');
				}
			},
			error : function(data) {
				console.log(data.msg);
			},
		});
	});
}