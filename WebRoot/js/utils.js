//做成工具的
/*对Date类的扩展，添加日期格式化的方法（将日期类型，转为指定格式的字符串）*/
//原型编程  Date.prototype-把属性加给这个类型--加format的方法
Date.prototype.format = function(fmt) {
	//对fmt格式的转换
	var o = {
			"M+" : this.getMonth() + 1,
			"d+" : this.getDate(),
			"h+" : this.getHours(),
			"m+" : this.getMinutes(),
			"s+" : this.getSeconds(),
			"S+" : this.getMilliseconds(),
	}
	// 年份特殊处理
	if(/(y+)/.test(fmt)) { // RegExp.$1 表示匹配的第一部分---有包含y
		//替换
		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4-RegExp.$1.length));//截取一个字符串
	}
	for(var key in o) {//把对象o取出来，进行处理---其他几个都转换掉
		if(new RegExp("(" + key + ")").test(fmt)) {
			//RegExp.$1--匹配的第一部分
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[key]) : (("00" + o[key]).substr(("" + o[key]).length)));
		}
	}
	return fmt;
}