$(function() {
	// 当表单提交时触发
	$("#myform").submit(function() {
		// 文件格式验证
		var filename = $("#uploadfile").val();
		// 验证非空
		if(filename === '') {
			layer.msg('请选择文件！');
			return false;
		}
		// 验证后缀
		var fileType = (filename.substring(filename.lastIndexOf('.')+1, filename.length)).toLowerCase();
		console.log('filename ---------- ' + filename);
		console.log('fileType ---------- ' + fileType);
		if(fileType != 'xls' && fileType != 'xlsx') {
			layer.msg('文件类型不正确，请选择Excel文件提交！');
			return false;
		}
		// 验证通过，提交表单
		$(this).ajaxSubmit({
			url : 'PoiExcelServlet',
			dataType : 'json',
			success : function(data) {
				if(data.result) {
					layer.msg('上传成功！');
				} else {
					layer.msg('上传失败！');
				}
			},
			error : function(data) {
				console.log(data.msg);
			},
		});
	});
})