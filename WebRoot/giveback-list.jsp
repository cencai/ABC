<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="renderer" content="webkit|ie-comp|ie-stand">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
		<meta http-equiv="Cache-Control" content="no-siteapp" />
		<!--[if lt IE 9]>
			<script type="text/javascript" src="lib/html5shiv.js"></script>
			<script type="text/javascript" src="lib/respond.min.js"></script>
		<![endif]-->
		<link rel="stylesheet" type="text/css" href="static/h-ui/css/H-ui.min.css" />
		<link rel="stylesheet" type="text/css" href="static/h-ui.admin/css/H-ui.admin.css" />
		<link rel="stylesheet" type="text/css" href="lib/Hui-iconfont/1.0.8/iconfont.css" />
		<link rel="stylesheet" type="text/css" href="static/h-ui.admin/skin/default/skin.css" id="skin" />
		<link rel="stylesheet" type="text/css" href="static/h-ui.admin/css/style.css" />
		<!--[if IE 6]>
			<script type="text/javascript" src="lib/DD_belatedPNG_0.0.8a-min.js" ></script>
			<script>DD_belatedPNG.fix('*');</script>
		<![endif]-->
		<title>归还列表</title>
	</head>
	<body>
		<nav class="breadcrumb">
			<i class="Hui-iconfont">&#xe67f;</i> 首页 
			<span class="c-gray en">&gt;</span> 归还管理 
			<span class="c-gray en">&gt;</span> 归还列表 
			<a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" >
				<i class="Hui-iconfont">&#xe68f;</i>
			</a>
		</nav>
		<div class="page-container">
			<div class="text-c"> 日期范围：
				<input type="text" onfocus="WdatePicker({ maxDate:'#F{$dp.$D(\'logmax\')||\'%y-%M-%d\'}' })" id="logmin" class="input-text Wdate" style="width:120px;">
				-
				<input type="text" onfocus="WdatePicker({ minDate:'#F{$dp.$D(\'logmin\')}',maxDate:'%y-%M-%d' })" id="logmax" class="input-text Wdate" style="width:120px;">
				<!--等下要取它的值，按照ID来  -->
				<input type="text" name="" id="searchConditions" placeholder=" 器具名称" style="width:250px" class="input-text">
				<button type="submit" name="" id="searchBtn" class="btn btn-success" ><i class="Hui-iconfont">&#xe665;</i> 查询归还</button>
			</div>
			
			<div class="cl pd-5 bg-1 bk-gray mt-20"> 
				<span class="l">
									<!--把点击事件抽出来,通过按钮来触发对应的事件 -->
					<a href="javascript:;" id="delGiveBacksBtn" class="btn btn-danger radius">
						<i class="Hui-iconfont">&#xe6e2;</i> 批量删除
					</a>
					<a class="btn btn-primary radius" id="addGiveBacksBtn" href="javascript:;">
						<i class="Hui-iconfont">&#xe600;</i> 添加归还信息
					</a>
				</span>
				<span class="r">共有数据：<strong>54</strong> 条</span> 
			</div>
			<div class="mt-20">
				<table class="table table-border table-bordered table-bg table-hover table-sort">
					<thead>
						<tr class="text-c">
							<th width="40">
								<input name="" type="checkbox" value="">
							</th>
							<th width="80">ID</th>
							<th width="100">器具名称</th>
							<th width="100">型号</th>
							<th width="80">规格</th>
							<th width="40">领用数量</th>
							<th width="100">领用人</th>
							<th width="150">领用日期</th>
							<th width="60">发布状态</th>
							<th width="100">备注</th>
							<th width="100">操作</th>
						</tr>
					</thead>
					<tbody>
					<!-- 把数据弄成动态显示，列定义在js中
						<tr class="text-c">
							<td><input name="" type="checkbox" value=""></td>
							<td>001</td>
							<td>名称</td>
							<td><a href="javascript:;" onClick="picture_edit('图库编辑','picture-show.jsp','10001')"><img width="210" class="picture-thumb" src="temp/200x150.jpg"></a></td>
							<td class="text-l"><a class="maincolor" href="javascript:;" onClick="picture_edit('图库编辑','picture-show.jsp','10001')">现代简约 白色 餐厅</a></td>
							<td class="text-c">标签</td>
							<td>2014-6-11 11:11:42</td>
							<td class="td-status">
								<span class="label label-success radius">已发布</span>
							</td>
							<td class="td-manage">
								<a style="text-decoration:none" onClick="picture_stop(this,'10001')" href="javascript:;" title="下架">
									<i class="Hui-iconfont">&#xe6de;</i>
								</a>
								<a style="text-decoration:none" class="ml-5" onClick="picture_edit('图库编辑','picture-add.jsp','10001')" href="javascript:;" title="编辑">
									<i class="Hui-iconfont">&#xe6df;</i>
								</a> 
								<a style="text-decoration:none" class="ml-5" onClick="picture_del(this,'10001')" href="javascript:;" title="删除">
									<i class="Hui-iconfont">&#xe6e2;</i>
								</a>
							</td>
						</tr>
					 -->
					</tbody>
				</table>
			</div>
		</div>
		
		<!--_footer 作为公共模版分离出去-->
		<script type="text/javascript" src="lib/jquery/1.9.1/jquery.min.js"></script> 
		<script type="text/javascript" src="lib/layer/2.4/layer.js"></script>
		<script type="text/javascript" src="static/h-ui/js/H-ui.min.js"></script> 
		<script type="text/javascript" src="static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->
		
		<!--请在下方写此页面业务相关的脚本-->
		<script type="text/javascript" src="lib/My97DatePicker/4.8/WdatePicker.js"></script> 
		<script type="text/javascript" src="lib/datatables/1.10.0/jquery.dataTables.min.js"></script> 
		<script type="text/javascript" src="lib/laypage/1.2/laypage.js"></script>
		<script type="text/javascript" src="js/utils.js"></script>	<!--日期处理  -->
		<script type="text/javascript" src="js/giveback-list.js"></script> <!--用到上面的日期  -->
	</body>
</html>
