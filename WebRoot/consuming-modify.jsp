<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="renderer" content="webkit|ie-comp|ie-stand">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
		<meta http-equiv="Cache-Control" content="no-siteapp" />
		<link rel="Bookmark" href="/favicon.ico" >
		<link rel="Shortcut Icon" href="/favicon.ico" />
		<!--[if lt IE 9]>
			<script type="text/javascript" src="lib/html5shiv.js"></script>
			<script type="text/javascript" src="lib/respond.min.js"></script>
		<![endif]-->
		<link rel="stylesheet" type="text/css" href="static/h-ui/css/H-ui.min.css" />
		<link rel="stylesheet" type="text/css" href="static/h-ui.admin/css/H-ui.admin.css" />
		<link rel="stylesheet" type="text/css" href="lib/Hui-iconfont/1.0.8/iconfont.css" />
		<link rel="stylesheet" type="text/css" href="static/h-ui.admin/skin/default/skin.css" id="skin" />
		<link rel="stylesheet" type="text/css" href="static/h-ui.admin/css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/cityLayout.css" /><!--城市插件  -->
		<!--[if IE 6]>
			<script type="text/javascript" src="lib/DD_belatedPNG_0.0.8a-min.js" ></script>
			<script>DD_belatedPNG.fix('*');</script>
		<![endif]-->
		<title>修改领用信息</title>
	</head>
	<body>
		<article class="page-container">
			<form action="" method="post" class="form form-horizontal" id="form-consuming-add">
				<input type="hidden" name="operate" value="modify" /> <!-- 隐藏值，作为操作的标记 -->
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-3">
						<span class="c-red">*</span>编号：
					</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text" class="input-text" value="" readonly="readonly" id="cId" name="cId" >
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-3">
						<span class="c-red">*</span>器具编号：
					</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text" class="input-text" value="" placeholder="" id="aId" name="aId">
					</div>
				</div>
				
				
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-3">
						<span class="c-red">*</span>数量：
					</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="number" value="" placeholder="" id="No" name="No">
					</div>
				</div>
				
				
				<div class="row cl">
				<label class="form-label col-xs-4 col-sm-2">
					<span class="c-red">*</span>领用人：
				</label>
				<div class="formControls col-xs-8 col-sm-9">
					<span class="select-box"> 
						<select name="uid" id="uid" class="select">
							<option value="0">1</option>
							<option value="1">1</option>
							<option value="11">11</option>
							<option value="12">12</option>
							<option value="13">13</option>
						</select>
					</span>
				</div>
			</div>
				
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-3">
						<span class="c-red">*</span>领用日期：
					</label>
					<div class="formControls col-xs-8 col-sm-9">
						<!--引用日期插件  -->
						<input type="text" class="input-text Wdate" placeholder="" onFocus="WdatePicker({lang:'zh-cn'})" name="consuDate" id="consuDate">
					</div>
				</div>
				
				<!-- <div class="row cl">
					<label class="form-label col-xs-4 col-sm-3">所在城市：</label>
					<div class="formControls col-xs-8 col-sm-9"> 
						<span class="select-box">
						<select class="select" size="1" name="city">
							<option value="" selected>请选择城市</option>
							<option value="1">北京</option>
							<option value="2">上海</option>
							<option value="3">广州</option>
						</select> --><!--引用城市插件 
							<input name="city" id="city"  type="text"  class="city_input input-text" readonly="readonly">
						</span> 
					</div>
				</div> -->
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-3">备注：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<textarea name="beizhu" id="beizhu" cols="" rows="" class="textarea"  placeholder="说点什么...最少输入10个字符"></textarea>
						<p class="textarea-numberbar"><em class="textarea-length">0</em>/100</p>
					</div>
				</div>
				<div class="row cl">
					<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
						<input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
					</div>
				</div>
			</form>
		</article>
		
		<!--_footer 作为公共模版分离出去-->
		<script type="text/javascript" src="lib/jquery/1.9.1/jquery.min.js"></script> 
		<script type="text/javascript" src="lib/layer/2.4/layer.js"></script>
		<script type="text/javascript" src="static/h-ui/js/H-ui.min.js"></script> 
		<script type="text/javascript" src="static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->
		
		<!--请在下方写此页面业务相关的脚本--> 
		<script type="text/javascript" src="lib/My97DatePicker/4.8/WdatePicker.js"></script>
		<script type="text/javascript" src="lib/jquery.validation/1.14.0/jquery.validate.js"></script> 
		<script type="text/javascript" src="lib/jquery.validation/1.14.0/validate-methods.js"></script> 
		<script type="text/javascript" src="lib/jquery.validation/1.14.0/messages_zh.js"></script>
		<script type="text/javascript" src="js/utils.js"></script><!--日期插件  -->
		<script type="text/javascript" src="js/consuming-modify.js"></script>
		
	</body>
</html>