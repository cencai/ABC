package com.ggy.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * 当访问登录页面时，查找是否保存Cookie，有保存则从cookie中获取保存的用户名和密码完成自动登录
 * @author Administrator
 *
 */
public class LoginCookieFilter implements Filter {

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		// 获取Cookie中保存的用户名和密码
		String username = "";
		String userpwd = "";
		// 筛选Cookie
		Cookie[] cookies = req.getCookies();
		if(cookies != null) {
			// 遍历
			for(Cookie cookie : cookies) {
				if("unameKey".equals(cookie.getName())) {
					username = cookie.getValue();
				}
				if("upwdKey".equals(cookie.getName())) {
					userpwd = cookie.getValue();
				}
			}
		}
		// 用保存的用户名和密码，自动完成登录验证
		if("".equals(username) || "".equals(userpwd)) {
			// 用户名或密码为空，则是没有保存，则继续进入登录页面
			chain.doFilter(request, response);
		} else {
			String url = "login?username=" + username + "&userpwd=" +userpwd;
			request.getRequestDispatcher(url).forward(request, response);
		}
	}

	@Override
	public void init(FilterConfig fConfig) throws ServletException {
	}

}
