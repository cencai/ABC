package com.ggy.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * 编码过滤器
 * @author Administrator
 *	采用配置的方式来实现，因为多个比较好控制加载的顺序----web.xml加上过滤器的配置
 */
public class CharacterEncodingFilter implements Filter {
	private String encodeName = "utf-8";//定一个属性，在init里面取值，在doFilter里面就可以用了

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		request.setCharacterEncoding(this.encodeName);
//		response.setCharacterEncoding(this.encodeName);
		//设置对应中文编码
		response.setContentType("text/html;charset=" + this.encodeName);
		//转到下一个请求
		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		//加载初始化的参数，把配置里面的编码读过来
		String initParam = fConfig.getInitParameter("encode");//fConfig-配置-fConfig.getInitParameter-获取初始化参数值
		this.encodeName = (initParam==null) ? this.encodeName : initParam;//设置值
	}

}
