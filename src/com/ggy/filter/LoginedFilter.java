package com.ggy.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.ggy.entity.User;

/**访问这个页面，判断有登录才让你进这个页面，没有登录就提示你要先登录，转到登录，进行过滤
 * Cookie是保存在客户端的小文本
 * 	1.保存内存中---容易丢失，浏览器关闭后就不存在了
 * 	2.保存在硬盘上，设置时长--基本都保存在硬盘上
 * 常见应用：
 * 	1.简化登录，在有效期内，不用登录直接到欢迎页面--先存到Cookie，在取到相应的值处理
 * 		容易造成信息的泄露
 * 	2.记录浏览器记录：
 * 
 * 保存及获取Cookie
 * 	既然是保存到客户端，肯定通过response跟着响应一起返回客户端，一起保存
 * response.addCookie(new Cookie)
 * 	要取通过请求，从请求对象去取
 * Cookie[] cookies=request.getCookies()
 * session---作用域对象，一次会话周期里的作用域对象---保存在服务端的，保存周期一次会话周期
 * 
 * 
 * 对某些页面的直接访问访问需要先进行登录的过滤
 * @author Administrator
 *
 */
public class LoginedFilter implements Filter {

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		/**进拦截器，判断是否有登录----有个保存这个人的信息，    登录成功保存在session里面的的这个人的信息，获取这个人
		 * httpsession是用request.getsession取得
		 *  但是这边找不到session，但是都是http协议，可以强转
		 */
		
		HttpServletRequest req = (HttpServletRequest) request;
		/**转成之后，现在要取的是user信息--------request.getsession()，现在就可以获取到了
		 * 存的是loginedUser这个人，按照这个人来取
		 */
		User user = (User) req.getSession().getAttribute("loginedUser");
		if(user == null) {//判断这个人是否存在
			// 还没有登录，先转到登录页面
			req.setAttribute("msg", "请先登录！");
			req.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}
		// 如果登录成功了，则转到下一个资源，(拦截的是index.jsp，所以下一个资源是index.jsp直接进入index.jsp)
		chain.doFilter(request, response);//用参数，不要用强转的req
	}

	@Override
	public void init(FilterConfig fConfig) throws ServletException {
	}

}
