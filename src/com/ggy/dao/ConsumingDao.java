package com.ggy.dao;

import java.util.List;
import java.util.Map;

import com.ggy.entity.Consuming;

/**
 * 领用管理数据访问层接口
 * @author Administrator
 *
 */
public interface ConsumingDao {
	
	/**
	 * 统计所有的领用数量
	 * 先查找所有
	 * @return
	 */
	public int selectAllConsumingCount();
	
	/**分页查找要显示的领用信息
	 * 查找的，得到所有领用资料，把它封装到list里面----带条件查找
	 * @param iDisplayStart iDisplayStart 从第几条开始显示(查找)
	 * @param iDisplayLength iDisplayLength 显示的数量[查找几条]
	 * @return
	 */
//	public List<Consuming> selectConsumings(int iDisplayStart, int iDisplayLength);
	
	/**带条件
	 * 
	 * @param condition
	 * @return
	 */
	public int selectConsumingCountByCondition(Map<String, String> condition);
	
	/**带条件
	 * 
	 * @param condition
	 * @param iDisplayStart
	 * @param iDisplayLength
	 * @return
	 */
	public List<Consuming> selectConsumingByCondition(Map<String, String> condition, int iDisplayStart, int iDisplayLength);
	/**delete from t_picture where pid in (?,?,?)
	 * 删除领用信息，支持批量删除		后面单个删除就可以用了，形成通用的
	 * 
	 * @param pids
	 * @return返回受影响的行数
	 */
	public int deleteConsuming(Integer[] cIds);
	
	/**
	 * 添加领用信息---添加整个对象
	 * @param consuming 
	 * @return
	 */
	public int insertConsuming(Consuming consuming);
	
	/**
	 * 修改领用信息
	 * @param consuming 传领用信息
	 * @return
	 */
	public int updateConsuming(Consuming consuming);
}
