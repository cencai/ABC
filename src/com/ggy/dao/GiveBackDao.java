package com.ggy.dao;

import java.util.List;
import java.util.Map;

import com.ggy.entity.GiveBack;


/**
 * 归还管理数据访问层接口
 * @author Administrator
 *
 */
public interface GiveBackDao {
	
	/**
	 * 统计所有的归还数量
	 * 先查找所有
	 * @return
	 */
//	public int selectAllGiveBackCount();
	
	/**分页查找要显示的归还信息
	 * 查找的，得到所有归还资料，把它封装到list里面----带条件查找
	 * @param iDisplayStart iDisplayStart 从第几条开始显示(查找)
	 * @param iDisplayLength iDisplayLength 显示的数量[查找几条]
	 * @return
	 */
//	public List<GiveBack> selectGiveBacks(int iDisplayStart, int iDisplayLength);
	
	/**带条件
	 * 
	 * @param condition
	 * @return
	 */
	public int selectGiveBackCountByCondition(Map<String, String> condition);
	
	/**带条件
	 * 
	 * @param condition
	 * @param iDisplayStart
	 * @param iDisplayLength
	 * @return
	 */
	public List<GiveBack> selectGiveBackByCondition(Map<String, String> condition, int iDisplayStart, int iDisplayLength);
	/**delete from t_picture where pid in (?,?,?)
	 * 删除信息，支持批量删除		后面单个删除就可以用了，形成通用的
	 * 
	 * @param pids
	 * @return返回受影响的行数
	 */
	public int deleteGiveBack(Integer[] gIds);
	
	/**
	 * 添加信息---添加整个对象
	 * @param GiveBack 
	 * @return
	 */
	public int insertGiveBack(GiveBack giveBack);
	
	/**
	 * 修改领用信息
	 * @param GiveBack 传领用信息
	 * @return
	 */
	public int updateGiveBack(GiveBack giveBack);
}
