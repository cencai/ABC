package com.ggy.dao;

import com.ggy.entity.User;

/**
 * 用户数据访问层接口
 * @author Administrator
 *
 */
public interface UserDAO {
	public User selectUserByUnamePwd(String uname, String upwd);
}
