package com.ggy.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ggy.dao.ConsumingDao;
import com.ggy.entity.Appliance;
import com.ggy.entity.Consuming;
import com.ggy.entity.User;
import com.ggy.util.JDBCUtil;

public class ConsumingDaoImpl implements ConsumingDao {

	/**
	 * 统计所有的领用数量
	 * 先查找所有
	 * @return
	 */
	@Override
	public int selectAllConsumingCount() {
		String sql="select count(1) from t_consuming";
		ResultSet rs=JDBCUtil.doQuery(sql);
		//只有一个结果
		try {
			if(rs.next()){
				return rs.getInt(1);//统计只有一列,没有其他结果
			}
		} catch (SQLException e) {
			System.out.println("查询出错...");
			e.printStackTrace();
		} finally {
			try {
					if(rs!=null)
						JDBCUtil.free(rs.getStatement().getConnection(), rs.getStatement(), rs);
			} catch (SQLException e) {
				System.out.println("关闭异常....");
				e.printStackTrace();
			}
		}
		return 0;
	}

	/**分页查找要显示的领用信息
	 * 查找的，得到所有领用资料，把它封装到list里面----带条件查找
	 * @param iDisplayStart iDisplayStart 从第几条开始显示(查找)
	 * @param iDisplayLength iDisplayLength 显示的数量[查找几条]
	 * @return
	 */
	/*@Override
	public List<Consuming> selectConsumings(int iDisplayStart,
			int iDisplayLength) {
		String sql="select c.*,ap.aName,ap.amodel,ap.standard,u.uname from t_consuming c,t_appliance ap,t_user u where c.aId=ap.aId and c.uId=u.uid limit ?,?";
		//结果存在list里面的
		List<Consuming> list=new ArrayList<Consuming>();
		iDisplayStart, iDisplayLength
		 * 从第几条开始找，每页显示几条，这个也是在变-----把它做成变量
		 
		ResultSet rs=JDBCUtil.doQuery(sql,iDisplayStart, iDisplayLength);
			try {
				while(rs.next()){
					//1.每次找到领用信息，封装成对象
					Consuming consuming=new Consuming();
					//设置参数
					consuming.setcId(rs.getInt("cId"));
					//型号，amodel规格，standard
					consuming.setAppliance(new Appliance(rs.getString("aname"), rs.getString("amodel"),rs.getString("standard")));
					
					
					consuming.setCount((rs.getInt("count")));
					
					consuming.setUser(new User(rs.getString("uname")));//领用人todo
					
					consuming.setConDate(rs.getTimestamp("conDate"));//Timestamp类型的日期
					consuming.setcStatus(rs.getInt("cStatus"));//状态
					consuming.setRemark(rs.getString("remark"));
					//2.加到list里面
					list.add(consuming);
				}
				return list;
			} catch (SQLException e) {
				System.out.println("查询出错~");
				e.printStackTrace();
			} finally {
				try {
					if(rs!=null)
					JDBCUtil.free(rs.getStatement().getConnection(), rs.getStatement(), rs);
				} catch (SQLException e) {
					System.out.println("关闭异常~");
					e.printStackTrace();
				}
			}
		return null;
	}
*/
	/**带条件
	 * 
	 * @param condition
	 * @return
	 */
	@Override
	public int selectConsumingCountByCondition(Map<String, String> condition) {
		//不知道你有几个条件，肯定不能直接都拼在这边啊，所以要用动态的  where后面是动态的,采用恒等的条件，后面那部分在来拼
				StringBuilder sql= new StringBuilder("select count(1) from t_consuming c,t_appliance ap,t_user u where c.aId=ap.aId and c.uId=u.uid");//关联user
				//取条件
				String logmin=condition.get("logmin");//2个框值-todo
				String logmax=condition.get("logmax"); 
				
				String searchConditions=condition.get("searchConditions");
				//封装对象
				Object[] params=new Object[5];
				int index=0;
				//拼接查询语句
				if(logmin !=null&& !"".equals(logmin)){
					sql.append(" and conDate>?");
					//同时把参数加进来
					params[index++]=logmin;
				}
				if(logmax !=null&& !"".equals(logmax)){
					sql.append(" and conDate<?");
					//同时把参数加进来
					params[index++]=logmin;
				}
				if(searchConditions !=null&& !"".equals(searchConditions)){
					sql.append(" and (aname like ?)");//todo
					//同时把参数加进来
					params[index++]="%"+searchConditions+"%";
				}
				ResultSet rs=JDBCUtil.doQuery(sql.toString(),params);
				//只有一个结果
						try {
							if(rs.next()){
								return rs.getInt(1);//统计只有一列,没有其他结果
							}
						} catch (SQLException e) {
							System.out.println("查询出错...");
							e.printStackTrace();
						} finally {
							try {
									if(rs!=null)
										JDBCUtil.free(rs.getStatement().getConnection(), rs.getStatement(), rs);
							} catch (SQLException e) {
								System.out.println("关闭异常....");
								e.printStackTrace();
							}
				}
				return 0;
	}

	/**带条件
	 * 多张表关联
	 * @param condition
	 * @param iDisplayStart
	 * @param iDisplayLength
	 * @return
	 */
	@Override
	public List<Consuming> selectConsumingByCondition(
			Map<String, String> condition, int iDisplayStart, int iDisplayLength) {
		List<Consuming> list=new ArrayList<Consuming>();
		StringBuilder sql=new StringBuilder("select c.*,ap.aName,ap.amodel,ap.standard,u.uname from t_consuming c,t_appliance ap,t_user u where c.aId=ap.aId and c.uId=u.uid");//关联user
		//取条件
		String logmin=condition.get("logmin");
		String logmax=condition.get("logmax"); 
		String searchConditions=condition.get("searchConditions");
		//封装对象
		Object[] params=new Object[7];//limit也算在内
		int index=0;
		//拼接查询语句
		if(logmin !=null&& "".equals(logmin)){
			sql.append(" and conDate>?");
			//同时把参数加进来
			params[index++]=logmin;
		}
		if(logmax !=null&& "".equals(logmax)){
			sql.append(" and conDate<?");
			//同时把参数加进来
			params[index++]=logmin;
		}
		if(searchConditions !=null&& "".equals(searchConditions)){
			sql.append(" and (aName like ?)");
			//同时把参数加进来
			params[index++]="%"+searchConditions+"%";
		}
		sql.append("limit ?,?");
		params[index++]=iDisplayStart;
		params[index++]=iDisplayLength;
		ResultSet rs=JDBCUtil.doQuery(sql.toString(),params);
		try {
			while(rs.next()){
				//1.每次找到领用信息，封装成对象
				Consuming consuming=new Consuming();
				//设置参数
				consuming.setcId(rs.getInt("cId"));
				
				//型号，amodel规格，standard
				consuming.setAppliance(new Appliance(rs.getString("aname"), rs.getString("amodel"),rs.getString("standard")));
				
				consuming.setCount((rs.getInt("count")));
				
				consuming.setUser(new User(rs.getString("uname")));//领用人todo
				
				consuming.setConDate(rs.getTimestamp("conDate"));//Timestamp类型的日期
				consuming.setcStatus(rs.getInt("cStatus"));//状态
				consuming.setRemark(rs.getString("remark"));
				//2.加到list里面
				list.add(consuming);
			}
			return list;
		} catch (SQLException e) {
			System.out.println("查询出错~");
			e.printStackTrace();
		} finally {
			try {
				if(rs!=null)
				JDBCUtil.free(rs.getStatement().getConnection(), rs.getStatement(), rs);
			} catch (SQLException e) {
				System.out.println("关闭异常~");
				e.printStackTrace();
			}
		}
		return null;
	}
	/**delete from t_picture where pid in (?,?,?)
	 * 删除领用信息，支持批量删除		后面单个删除就可以用了，形成通用的
	 * 
	 * @param pids
	 * @return返回受影响的行数
	 */
	@Override
	public int deleteConsuming(Integer[] cIds) {
		if(cIds.length>0){
			StringBuilder sql=new StringBuilder("delete from t_consuming where cId in (");
			for(int i=0;i<cIds.length;i++){
				sql.append("?,");//多了一个？，和少一个右括号，直接替换掉
			}
			sql.replace(sql.length()-1,sql.length(), ")");
			return JDBCUtil.doUpdate(sql.toString(), cIds);
			
		}
		return 0;
	}
	/**
	 * 添加领用信息---添加整个对象
	 * @param consuming 
	 * @return
	 */
	@Override
	public int insertConsuming(Consuming consuming) {
//		 String sql="insert into t_consuming(aname,amodel,standard,count,uId,conDate,remark) values(?,?,?,?,?)";//不用跟数据库的列名一样，只要参数一一对应即可
//			return JDBCUtil.doUpdate(sql,consuming.getAppliance().getaName(),consuming.getAppliance().getAmodel(),consuming.getAppliance().getStandard(),consuming.getCount(),consuming.getUser().getUname(),consuming.getConDate(),consuming.getRemark());//todo
		 String sql="insert into t_consuming(aId,count,uId,conDate,remark) values(?,?,?,?,?)";//不用跟数据库的列名一样，只要参数一一对应即可
		 	return JDBCUtil.doUpdate(sql,consuming.getAppliance().getaId(),consuming.getCount(),consuming.getUser().getUid(),consuming.getConDate(),consuming.getRemark());
	}
	
	/**
	 * 修改领用信息
	 * @param consuming 传领用信息
	 * @return
	 */
	@Override
	public int updateConsuming(Consuming consuming) {
		/*String sql="update t_consuming set aname=?,amodel=?,standard=?,count=?,conDate=?,remark=? where cId=?";
		return JDBCUtil.doUpdate(sql,consuming.getAppliance().getaName(),consuming.getAppliance().getAmodel(),consuming.getAppliance().getStandard(),consuming.getCount(),consuming.getConDate(),consuming.getRemark(),consuming.getcId());*/
		String sql="update t_consuming set aId=?,count=?,uId=?,conDate=?,remark=? where cId=?";//不用跟数据库的列名一样，只要参数一一对应即可
	 	return JDBCUtil.doUpdate(sql,consuming.getAppliance().getaId(),consuming.getCount(),consuming.getUser().getUid(),consuming.getConDate(),consuming.getRemark(),consuming.getcId());
	}

}
