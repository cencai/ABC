package com.ggy.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ggy.dao.GiveBackDao;
import com.ggy.entity.Appliance;
import com.ggy.entity.GiveBack;
import com.ggy.entity.User;
import com.ggy.util.JDBCUtil;

public class GiveBackDaoImpl implements GiveBackDao {


	/**带条件
	 * 
	 * @param condition
	 * @return
	 */
	@Override
	public int selectGiveBackCountByCondition(Map<String, String> condition) {
		//不知道你有几个条件，肯定不能直接都拼在这边啊，所以要用动态的  where后面是动态的,采用恒等的条件，后面那部分在来拼
		StringBuilder sql= new StringBuilder("select count(1) from t_giveback g,t_appliance ap,t_user u where g.aId=ap.aId and g.uId=u.uid");
		//取条件
		String logmin=condition.get("logmin");//2个框值-todo
		String logmax=condition.get("logmax"); 
		
		String searchConditions=condition.get("searchConditions");
		//封装对象
		Object[] params=new Object[5];
		int index=0;
		//拼接查询语句
		if(logmin !=null && !"".equals(logmin)){
			sql.append(" and giveDate>?");
			//同时把参数加进来
			params[index++]=logmin;
		}
		if(logmax !=null && !"".equals(logmax)){
			sql.append(" and giveDate<?");
			//同时把参数加进来
			params[index++]=logmin;
		}
		if(searchConditions !=null && !"".equals(searchConditions)){
			sql.append(" and (aname like ?)");//todo
			//同时把参数加进来
			params[index++]="%"+searchConditions+"%";
		}
		ResultSet rs=JDBCUtil.doQuery(sql.toString(),params);
		//只有一个结果
				try {
					if(rs.next()){
						return rs.getInt(1);//统计只有一列,没有其他结果
					}
				} catch (SQLException e) {
					System.out.println("查询出错...");
					e.printStackTrace();
				} finally {
					try {
							if(rs !=null)
								JDBCUtil.free(rs.getStatement().getConnection(), rs.getStatement(), rs);
					} catch (SQLException e) {
						System.out.println("关闭异常....");
						e.printStackTrace();
					}
		}
		return 0;
	}

	/**带条件
	 * 
	 * @param condition
	 * @param iDisplayStart
	 * @param iDisplayLength
	 * @return
	 */
	@Override
	public List<GiveBack> selectGiveBackByCondition(
			Map<String, String> condition, int iDisplayStart, int iDisplayLength) {
		List<GiveBack> list=new ArrayList<GiveBack>();
		StringBuilder sql=new StringBuilder("select g.*,ap.aName,ap.amodel,ap.standard,u.uname from t_giveback g,t_appliance ap,t_user u where g.aId=ap.aId and g.uId=u.uid");
		//取条件
		String logmin=condition.get("logmin");
		String logmax=condition.get("logmax"); 
		String searchConditions=condition.get("searchConditions");
		//封装对象
		Object[] params=new Object[7];//limit也算在内
		int index=0;
		//拼接查询语句
		if(logmin !=null && !"".equals(logmin)){
			sql.append(" and giveDate>?");
			//同时把参数加进来
			params[index++]=logmin;
		}
		if(logmax !=null && !"".equals(logmax)){
			sql.append(" and giveDate<?");
			//同时把参数加进来
			params[index++]=logmin;
		}
		if(searchConditions !=null && !"".equals(searchConditions)){
			sql.append(" and (aname like ?)");
			//同时把参数加进来
			params[index++]="%"+searchConditions+"%";
		}
		sql.append(" limit ?,?");
		params[index++]=iDisplayStart;
		params[index++]=iDisplayLength;
		ResultSet rs=JDBCUtil.doQuery(sql.toString(),params);
		try {
			while(rs.next()){
				//1.每次找到领用信息，封装成对象
				GiveBack giveBack=new GiveBack();
				//设置参数
				giveBack.setgId(rs.getInt("gId"));
				
				/*consuming.getAppliance().setaName((rs.getString("aname")));
				consuming.getAppliance().setAmodel((rs.getString("amodel")));//型号，amodel
				consuming.getAppliance().setStandard((rs.getString("standard")));//规格，standard
				*/
				//型号，amodel规格，standard
				giveBack.setAppliance(new Appliance(rs.getString("aname"), rs.getString("amodel"),rs.getString("standard")));
				
				giveBack.setCount((rs.getInt("count")));
				
				giveBack.setUser(new User(rs.getString("uname")));
				
				giveBack.setGiveDate(rs.getTimestamp("giveDate"));//Timestamp类型的日期
				giveBack.setgStatus(rs.getInt("gStatus"));//状态
				giveBack.setRemark(rs.getString("remark"));
				//2.加到list里面
				list.add(giveBack);
			}
			return list;
		} catch (SQLException e) {
			System.out.println("查询出错~");
			e.printStackTrace();
		} finally {
			try {
				if(rs!=null)
				JDBCUtil.free(rs.getStatement().getConnection(), rs.getStatement(), rs);
			} catch (SQLException e) {
				System.out.println("关闭异常~");
				e.printStackTrace();
			}
		}
		return null;
	}
	/**delete from t_picture where pid in (?,?,?)
	 * 删除领用信息，支持批量删除		后面单个删除就可以用了，形成通用的
	 * 
	 * @param pids
	 * @return返回受影响的行数
	 */
	@Override
	public int deleteGiveBack(Integer[] gIds) {
		if(gIds.length>0){
			StringBuilder sql=new StringBuilder("delete from t_giveback where gId in (");
			for(int i=0;i<gIds.length;i++){
				sql.append("?,");//多了一个？，和少一个右括号，直接替换掉
			}
			sql.replace(sql.length()-1,sql.length(), ")");
			return JDBCUtil.doUpdate(sql.toString(), gIds);
			
		}
		return 0;
	}
	/**
	 * 添加归还信息---添加整个对象
	 * @param GiveBack 
	 * @return
	 */
	@Override
	public int insertGiveBack(GiveBack giveBack) {//todo
		 String sql="insert into t_giveback(aname,amodel,standard,count,uid,giveDate,remark) values(?,?,?,?,?,?,?)";//不用跟数据库的列名一样，只要参数一一对应即可
			return JDBCUtil.doUpdate(sql,giveBack.getAppliance().getaName(),giveBack.getAppliance().getAmodel(),giveBack.getAppliance().getStandard(),giveBack.getCount(),giveBack.getUser().getUid(),giveBack.getGiveDate(),giveBack.getRemark());
	}
	
	/**
	 * 修改领用信息
	 * @param consuming 传领用信息
	 * @return
	 */
	@Override
	public int updateGiveBack(GiveBack giveBack) {
		String sql="update t_giveback set aname,amodel,standard,count=?,conDate=?,remark=? where gId=?";
		return JDBCUtil.doUpdate(sql,giveBack.getAppliance().getaName(),giveBack.getAppliance().getAmodel(),giveBack.getAppliance().getStandard(),giveBack.getCount(),giveBack.getGiveDate(),giveBack.getRemark(),giveBack.getgId());
	}

}
