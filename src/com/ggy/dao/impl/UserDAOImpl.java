package com.ggy.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.ggy.dao.UserDAO;
import com.ggy.entity.User;
import com.ggy.util.JDBCUtil;

public class UserDAOImpl implements UserDAO {

	@Override
	public User selectUserByUnamePwd(String uname, String upwd) {
		String sql = "select * from t_user where uname=? and upwd=?";
		ResultSet rs = JDBCUtil.doQuery(sql, uname, upwd);
		try {
			if(rs.next()) {
				User user = new User(uname, upwd);
				user.setUid(rs.getInt("uid"));
				user.setAddress(rs.getString("address"));
				user.setAge(rs.getInt("age"));
				return user;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(rs != null)
					JDBCUtil.free(rs.getStatement().getConnection(), rs.getStatement(), rs);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

}
