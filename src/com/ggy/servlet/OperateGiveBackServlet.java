package com.ggy.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ggy.entity.Appliance;
import com.ggy.entity.GiveBack;
import com.ggy.entity.User;
import com.ggy.service.GiveBackService;
import com.ggy.service.impl.GiveBackServiceImpl;
import com.google.gson.Gson;

@WebServlet("/OperateGiveBackServlet")
public class OperateGiveBackServlet extends HttpServlet {
	private GiveBackService giveBackService =new GiveBackServiceImpl();
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 获取要执行的操作,不是传了一个参数，根据这个参数来取
				String operate = request.getParameter("operate");
				PrintWriter out = response.getWriter();
				Gson gson = new Gson();//返回对应的数据
				//返回的结果【数据】保存到map里面
				Map<String, Object> data = new HashMap<String, Object>();
				if("delete".equals(operate)) {//判断要执行的删除
					//传过来是一个数组
					String[] params = request.getParameterValues("delGId");
					Integer[] gIds = new Integer[params.length];
					for(int i=0; i<params.length; i++) {
						//参数传的是int类型，转换下
						gIds[i] = Integer.parseInt(params[i]);
					}
					data.put("result", giveBackService.deleteGiveBacks(gIds)); // 调用删除，并保存结果
				} else {
					//得到的参数封装到这里  			aName,amodel,standard,count,uId,conDate,cStatus,remark
					GiveBack giveBack = new GiveBack();
					giveBack.setAppliance(new Appliance());
					giveBack.getAppliance().setaName(request.getParameter("aName"));
					giveBack.getAppliance().setAmodel(request.getParameter("amodel"));
					giveBack.getAppliance().setStandard(request.getParameter("standard"));
					if("add".equals(operate)) {
						// 添加图片
						data.put("result", giveBackService.addGiveBack(giveBack));
					} else if("modify".equals(operate)) {
						// 修改图片信息
						System.out.println(request.getParameter("giveDate"));
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
						try {
							giveBack.setUser(new User());
							giveBack.getUser().setUid(Integer.parseInt(request.getParameter("uid")));
							giveBack.setCount(Integer.parseInt(request.getParameter("count")));
							giveBack.setGiveDate(new Timestamp(df.parse(request.getParameter("giveDate")).getTime()));
							data.put("result", giveBackService.modifyGiveBack(giveBack));
						} catch (ParseException e) {
						}catch(Exception e){
							data.put("result", false);//转换成功，才改
							System.out.println("参数转换异常");
						}
					}
				}
				
				out.println(gson.toJson(data));//data数据返回回去
				out.flush();
				out.close();
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doGet(request, response);
	}
}
