package com.ggy.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ggy.service.GiveBackService;
import com.ggy.service.impl.GiveBackServiceImpl;
import com.google.gson.Gson;

/**
 * 领用管理控制层
 * @author Administrator
 *
 */
@WebServlet("/GetGiveBackServlet")
public class GetGiveBackServlet extends HttpServlet{
	private GiveBackService giveBackService=new GiveBackServiceImpl();
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
			//1.获取两个参数-----参数取回来都是String类型,从request上面获取的参数
				int iDisplayStart=0;
				int iDisplayLength=5;
				try {
					//转换的时候有可能出错
					iDisplayStart=Integer.parseInt(request.getParameter("iDisplayStart"));
					iDisplayLength=Integer.parseInt(request.getParameter("iDisplayLength"));
					
					
				} catch (NumberFormatException e) {
					System.out.println("参数获取失败，转换异常.....");
					e.printStackTrace();
				}
				//带条件查询,既然在js中设置参数，就继续取参数 取查询参数，把他封装到Map中
				Map<String,String> condition=new HashMap<String,String>();
				condition.put("logmin", request.getParameter("logmin")); 
				condition.put("logmax", request.getParameter("logmax"));
				condition.put("searchConditions", request.getParameter("searchConditions"));
				
				/**返回json/gson都可以，到页面中----把gson的包导入WEB-INF里面
				 * Gson实现对象的序列化，Object--->json格式的String类型
				 * 
				 */
				//获取对象
				Object giveBacks= giveBackService.getGiveBacks(condition,iDisplayStart,iDisplayLength);
				
				Gson gson=new Gson();//创建
				//对象输出--现在不是跳转页面，做异步处理，让我感觉页面不刷新页面---局部刷新，请求部分数据，会过来显示
				PrintWriter out=response.getWriter();
				//out把数据写出去
				out.println(gson.toJson(giveBacks));//把对象转成json格式的字符串【toJson】
				out.flush();
				out.close();
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doGet(request, response);
	}
}
