package com.ggy.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ggy.entity.Appliance;
import com.ggy.entity.Consuming;
import com.ggy.entity.User;
import com.ggy.service.ConsumingService;
import com.ggy.service.impl.ConsumingServiceImpl;
import com.google.gson.Gson;

@WebServlet("/OperateConsumingServlet")
public class OperateConsumingServlet extends HttpServlet {
	private ConsumingService consumingService =new ConsumingServiceImpl();
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 获取要执行的操作,不是传了一个参数，根据这个参数来取
				String operate = request.getParameter("operate");
				PrintWriter out = response.getWriter();
				Gson gson = new Gson();//返回对应的数据
				//返回的结果【数据】保存到map里面
				Map<String, Object> data = new HashMap<String, Object>();
				if("delete".equals(operate)) {//判断要执行的删除
					//传过来是一个数组
					String[] params = request.getParameterValues("delCId");
					Integer[] cIds = new Integer[params.length];
					for(int i=0; i<params.length; i++) {
						//参数传的是int类型，转换下
						cIds[i] = Integer.parseInt(params[i]);
					}
					data.put("result", consumingService.deleteConsumings(cIds)); // 调用删除，并保存结果
				} else {
					/*//得到的参数封装到这里----共同的属性  			aName,amodel,standard,count,uId,conDate,cStatus,remark
					Consuming consuming = new Consuming();
					consuming.setAppliance(new Appliance());//设置一个盒子装appliance对象----关联的
					consuming.getAppliance().setaName(request.getParameter("appName"));
					consuming.getAppliance().setAmodel(request.getParameter("amodel"));
					consuming.getAppliance().setStandard(request.getParameter("standard"));
					consuming.setUser(new User());//设置一个盒子装user对象--关联
					consuming.getUser().setUname(request.getParameter("username"));//todo,与页面对应
					consuming.setRemark(request.getParameter("beizhu"));*/
					//得到的参数封装到这里----共同的属性
					Consuming consuming=new Consuming();
					
					try {
						consuming.setAppliance(new Appliance());//作为一个容器
						consuming.setUser(new User());//作为一个容器
						consuming.getAppliance().setaId(Integer.parseInt(request.getParameter("aId")));
						consuming.getUser().setUid(Integer.parseInt(request.getParameter("uid")));
						consuming.setCount(Integer.parseInt(request.getParameter("No")));//
						System.out.println(request.getParameter("consuDate"));
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
						consuming.setConDate(new Timestamp(df.parse(request.getParameter("consuDate")).getTime()));//todo
						consuming.setRemark(request.getParameter("beizhu"));
					} catch (ParseException e) {
					}catch(Exception e){
						data.put("result", false);//转换成功，才改
						System.out.println("参数转换异常");
						e.printStackTrace();
					}
					if("add".equals(operate)) {
						// 添加领用信息
						data.put("result", consumingService.addConsuming(consuming));
					} else if("modify".equals(operate)) {
						// 修改领用信息
						consuming.setcId(Integer.parseInt(request.getParameter("cId")));
						data.put("result", consumingService.modifyConsuming(consuming));
					}
					
					
				}
				
				out.println(gson.toJson(data));//data数据返回回去
				out.flush();
				out.close();
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doGet(request, response);
	}
}
