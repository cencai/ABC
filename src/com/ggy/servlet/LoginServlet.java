package com.ggy.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ggy.service.UserService;
import com.ggy.service.impl.UserServiceImpl;

/**
 * 用户登录的控制层
 * 
 * @author Administrator
 *
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private UserService userService = new UserServiceImpl();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		this.doPost(request, response);
	}
	
	//登录只允许post，因为用get提交，会把用户名和密码暴露出来
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		//省略中文编码，已经把中文编码做成编码过滤器
		// 获取用户名密码参数
		String username = request.getParameter("username");
		String userpwd = request.getParameter("userpwd");
		
		
		// 验证登录
		if(userService.login(username, userpwd)) {
			// 只保存正确的登录信息
			String online = request.getParameter("online"); // 是否勾选保持登录状态
//			System.out.println("online ========= " + online);
			if(online != null) {
				System.out.println("勾选保持登录状态，需要保存Cookie。。。");
				// 保存用户名和密码
				Cookie unameCk = new Cookie("unameKey", username);
				Cookie upwdCk = new Cookie("upwdKey", userpwd);
				// 设置有效期，将cookie保存到硬盘
				unameCk.setMaxAge(24*3600); // 单位是秒
				upwdCk.setMaxAge(24*3600); // 单位是秒
				// 添加响应，保存到客户端
				response.addCookie(unameCk);
				response.addCookie(upwdCk);
			}
			
			// 保存，统计访问数量
//			Object obj = this.getServletContext().getAttribute("curCount");
//			int visitCount = (obj != null) ? (int)obj : 0;
/*			int visitCount = (int) this.getServletContext().getAttribute("curCount"); // 数据库中一定有值，curCount在监听器被初始化
			this.getServletContext().setAttribute("curCount", ++visitCount);*/
			
			// 登录成功，保存用户名信息
			HttpSession session = request.getSession();
			session.setAttribute("loginedUser", userService.getLoginedUser());
			// 只保存用户名
//			session.setAttribute("username", userService.getLoginedUser().getUname());
//			session.setAttribute("username", username);
			request.getRequestDispatcher("index.jsp").forward(request, response);
		} else {
			request.setAttribute("msg", "登录失败，用户名或密码错误！");
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}
		
	}
}
