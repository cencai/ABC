package com.ggy.service;

import com.ggy.entity.User;

/**
 * 用户业务处理层接口
 * @author Administrator
 *
 */
public interface UserService {
	public boolean login(String uname, String upwd);
	public User getLoginedUser();
}
