package com.ggy.service.impl;

import com.ggy.dao.UserDAO;
import com.ggy.dao.impl.UserDAOImpl;
import com.ggy.entity.User;
import com.ggy.service.UserService;

public class UserServiceImpl implements UserService {

	private UserDAO userDAO = new UserDAOImpl();
	private User loginedUser; // 保存已登录成功的用户信息

	public User getLoginedUser() {
		return loginedUser;
	}

	@Override
	public boolean login(String uname, String upwd) {
		User user = userDAO.selectUserByUnamePwd(uname, upwd);
		// 判断登录成功与否
		if(user != null) {
			loginedUser = user;
			return true;
		}
		return false;
	}

}
