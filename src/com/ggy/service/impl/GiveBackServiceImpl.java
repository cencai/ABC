package com.ggy.service.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

import com.ggy.dao.GiveBackDao;
import com.ggy.dao.impl.GiveBackDaoImpl;
import com.ggy.entity.GiveBack;
import com.ggy.entity.PageVO;
import com.ggy.service.GiveBackService;

public class GiveBackServiceImpl implements GiveBackService {
	
	private GiveBackDao giveBackDao=new GiveBackDaoImpl();
	
	

	/**
	 * 带条件
	 * @param condition
	 * @param iDisplayStart
	 * @param iDisplayLenth
	 * @return
	 */
	@Override
	public PageVO<GiveBack> getGiveBacks(Map<String, String> condition,
			int iDisplayStart, int iDisplayLength) {
				//1.封装一个对象,创建页面对象
				PageVO<GiveBack> page=new PageVO<GiveBack>(iDisplayStart,iDisplayLength);
				//2.获取总记录数---要有一个dao层
				page.setiTotalRecords(giveBackDao.selectGiveBackCountByCondition(condition));
				//3.获取当前页显示的数据
				page.setAaData(giveBackDao.selectGiveBackByCondition(condition, iDisplayStart, iDisplayLength));
				return page;
	}

	@Override
	public boolean deleteGiveBacks(Integer[] gIds) {
		return giveBackDao.deleteGiveBack(gIds)>0;//返回true
	}

	@Override
	public boolean addGiveBack(GiveBack giveBack) {
		giveBack.setGiveDate(new Timestamp(new Date().getTime()));//设置当前时间
		return giveBackDao.insertGiveBack(giveBack)>0;
	}

	@Override
	public boolean modifyGiveBack(GiveBack giveBack) {
		return giveBackDao.updateGiveBack(giveBack)>0;
	}

}
