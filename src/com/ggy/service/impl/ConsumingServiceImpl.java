package com.ggy.service.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

import com.ggy.dao.ConsumingDao;
import com.ggy.dao.impl.ConsumingDaoImpl;
import com.ggy.entity.Consuming;
import com.ggy.entity.PageVO;
import com.ggy.service.ConsumingService;

public class ConsumingServiceImpl implements ConsumingService {
	
	private ConsumingDao consumingDao=new ConsumingDaoImpl();
	
	/**返回页面，每页的数据以及你当前现在是第几页，以及你要显示相关的数据，一共有多少条
	 * 自己封装一个显示数据的这个对象
	 * @param iDisplayStart 从页面过来，所以这边也要传过来的
	 * @param iDisplayLenth 
	 * @return返回一个list---现在不只是返回集合,
	 * 					还包含其他分页的信息
	 */
	@Override
	public PageVO<Consuming> getConsumings(int iDisplayStart, int iDisplayLength) {
				//1.封装一个对象,创建页面对象
				PageVO<Consuming> page=new PageVO<Consuming>(iDisplayStart,iDisplayLength);
				//2.获取总记录数---要有一个dao层
				page.setiTotalRecords(consumingDao.selectAllConsumingCount());
				
				//3.获取当前页显示的数据
//				page.setAaData(consumingDao.selectConsumings(iDisplayStart,iDisplayLength));
				return page;
	}

	/**
	 * 带条件
	 * @param condition
	 * @param iDisplayStart
	 * @param iDisplayLenth
	 * @return
	 */
	@Override
	public PageVO<Consuming> getConsumings(Map<String, String> condition,
			int iDisplayStart, int iDisplayLength) {
				//1.封装一个对象,创建页面对象
				PageVO<Consuming> page=new PageVO<Consuming>(iDisplayStart,iDisplayLength);
				//2.获取总记录数---要有一个dao层
				page.setiTotalRecords(consumingDao.selectConsumingCountByCondition(condition));
				//3.获取当前页显示的数据
				page.setAaData(consumingDao.selectConsumingByCondition(condition, iDisplayStart, iDisplayLength));
				return page;
	}

	@Override
	public boolean deleteConsumings(Integer[] cIds) {
		return consumingDao.deleteConsuming(cIds)>0;//返回true
	}

	@Override
	public boolean addConsuming(Consuming consuming) {
		consuming.setConDate(new Timestamp(new Date().getTime()));//设置当前时间
		return consumingDao.insertConsuming(consuming)>0;
	}

	@Override
	public boolean modifyConsuming(Consuming consuming) {
		return consumingDao.updateConsuming(consuming)>0;
	}

}
