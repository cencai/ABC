package com.ggy.service;

import java.util.Map;

import com.ggy.entity.Consuming;
import com.ggy.entity.PageVO;

/**
 * 领用管理业务处理层
 * @author Administrator
 *
 */
public interface ConsumingService {
	/**返回页面，每页的数据以及你当前现在是第几页，以及你要显示相关的数据，一共有多少条
	 * 自己封装一个显示数据的这个对象
	 * @param iDisplayStart 从页面过来，所以这边也要传过来的
	 * @param iDisplayLenth 
	 * @return返回一个list---现在不只是返回集合,
	 * 					还包含其他分页的信息
	 */
	public PageVO<Consuming> getConsumings(int iDisplayStart,int iDisplayLength);
	
	/**
	 * 带条件
	 * @param condition
	 * @param iDisplayStart
	 * @param iDisplayLenth
	 * @return
	 */
	public PageVO<Consuming> getConsumings(Map<String ,String> condition,int iDisplayStart,int iDisplayLength);
	
	public boolean deleteConsumings(Integer[] cIds);
	
	public boolean addConsuming(Consuming consuming);
	public boolean modifyConsuming(Consuming consuming);
}
