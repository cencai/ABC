package com.ggy.service;

import java.util.Map;

import com.ggy.entity.GiveBack;
import com.ggy.entity.PageVO;

/**
 * 归还管理业务处理层
 * @author Administrator
 *
 */
public interface GiveBackService {
	/**返回页面，每页的数据以及你当前现在是第几页，以及你要显示相关的数据，一共有多少条
	 * 自己封装一个显示数据的这个对象
	 * @param iDisplayStart 从页面过来，所以这边也要传过来的
	 * @param iDisplayLenth 
	 * @return返回一个list---现在不只是返回集合,
	 * 					还包含其他分页的信息
	 */
//	public PageVO<GiveBack> getGiveBacks(int iDisplayStart,int iDisplayLength);
	
	/**
	 * 带条件
	 * @param condition
	 * @param iDisplayStart
	 * @param iDisplayLenth
	 * @return
	 */
	public PageVO<GiveBack> getGiveBacks(Map<String ,String> condition,int iDisplayStart,int iDisplayLength);
	
	public boolean deleteGiveBacks(Integer[] gIds);
	
	public boolean addGiveBack(GiveBack giveBack);
	public boolean modifyGiveBack(GiveBack giveBack);
}
