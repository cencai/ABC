package com.ggy.entity;

/**
 * MVC
  		M-数据模型--使用java类实现业务逻辑，分成3层
  			分别是实体类[User.java] 数据处理类[UserDao] 业务逻辑的服务类[loginService.java]
  		V-用户界面--使用JSP实现视图，其中使用到JS校验	C-控制器--使用Servlet接收JSP的请求，获得请求参数，调用Java类中的业务逻辑方法，根据结果不同，跳转到不同的JSP页面
 * 用户实体类
 * 
 * @author Administrator
 *
 */
public class User {
	private int uid;
	private String uname;
	private String upwd;
	private String address;
	private int age;

	public User() {
		super();
	}

	//uid是自增的，address和age是可以为空的
	public User(String uname) {
		super();
		this.uname = uname;
	}

	//uid是自增的，address和age是可以为空的
	public User(String uname, String upwd) {
		super();
		this.uname = uname;
		this.upwd = upwd;
	}

	public User(int uid, String uname, String upwd, String address, int age) {
		super();
		this.uid = uid;
		this.uname = uname;
		this.upwd = upwd;
		this.address = address;
		this.age = age;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getUpwd() {
		return upwd;
	}

	public void setUpwd(String upwd) {
		this.upwd = upwd;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "User [uid=" + uid + ", uname=" + uname + ", upwd=" + upwd
				+ ", address=" + address + ", age=" + age + "]";
	}

}
