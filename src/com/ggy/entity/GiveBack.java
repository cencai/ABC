package com.ggy.entity;

import java.sql.Timestamp;

/**
 * 归还实体类
 * 
 * @author Administrator
 *
 */
public class GiveBack {
	private int gId;// 自增
	private Appliance appliance;// 名称、型号、规格
	private int count;
	private User user;// 归还人
	private Timestamp giveDate;
	private int gStatus;//状态，为空
	private String remark;//备注，为空

	public GiveBack() {
		super();
	}

	public GiveBack(int gId, Appliance appliance, int count, User user,
			Timestamp giveDate, int gStatus, String remark) {
		super();
		this.gId = gId;
		this.appliance = appliance;
		this.count = count;
		this.user = user;
		this.giveDate = giveDate;
		this.gStatus = gStatus;
		this.remark = remark;
	}

	public int getgId() {
		return gId;
	}

	public void setgId(int gId) {
		this.gId = gId;
	}

	public Appliance getAppliance() {
		return appliance;
	}

	public void setAppliance(Appliance appliance) {
		this.appliance = appliance;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Timestamp getGiveDate() {
		return giveDate;
	}

	public void setGiveDate(Timestamp giveDate) {
		this.giveDate = giveDate;
	}

	public int getgStatus() {
		return gStatus;
	}

	public void setgStatus(int gStatus) {
		this.gStatus = gStatus;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "GiveBack [gId=" + gId + ", appliance=" + appliance + ", count="
				+ count + ", user=" + user + ", giveDate=" + giveDate
				+ ", gStatus=" + gStatus + ", remark=" + remark + "]";
	}

}
