package com.ggy.entity;

import java.sql.Timestamp;

/**
 * 领用实体类
 * 
 * @author Administrator
 *
 */
public class Consuming {
	private int cId;
	private Appliance appliance;// 名称、型号、规格、
	private int count;// 数量
	private User user;// 领用人
	private Timestamp conDate;// 日期
	private int cStatus;//状态，为空
	private String remark;// 备注，为空

	public Consuming() {
		super();
	}

	public Consuming(int cId, Appliance appliance, int count, User user,
			Timestamp conDate, int cStatus, String remark) {
		super();
		this.cId = cId;
		this.appliance = appliance;
		this.count = count;
		this.user = user;
		this.conDate = conDate;
		this.cStatus = cStatus;
		this.remark = remark;
	}

	public int getcId() {
		return cId;
	}

	public void setcId(int cId) {
		this.cId = cId;
	}

	public Appliance getAppliance() {
		return appliance;
	}

	public void setAppliance(Appliance appliance) {
		this.appliance = appliance;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Timestamp getConDate() {
		return conDate;
	}

	public void setConDate(Timestamp conDate) {
		this.conDate = conDate;
	}

	public int getcStatus() {
		return cStatus;
	}

	public void setcStatus(int cStatus) {
		this.cStatus = cStatus;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "Consuming [cId=" + cId + ", appliance=" + appliance
				+ ", count=" + count + ", user=" + user + ", conDate="
				+ conDate + ", cStatus=" + cStatus + ", remark=" + remark + "]";
	}

}
