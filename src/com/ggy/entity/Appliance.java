package com.ggy.entity;

/**
 * 器具明细表
 * 
 * @author Administrator
 *
 */
public class Appliance {
	private int aId;// 自增
	private String aName;
	private String standard;// 规格
	private String amodel;// 型号
	private double accuracy;// 精度，为空
	private String contractor;// 厂商，为空
	private User user;// 制单人
	private String remark;// 备注,为空

	public Appliance() {
		super();
	}

	public Appliance(String aName, String amodel,String standard) {
		super();
		this.aName = aName;
		this.amodel = amodel;
		this.standard = standard;
	}

	public Appliance(int aId, String aName, String standard, String amodel,
			double accuracy, String contractor, User user, String remark) {
		super();
		this.aId = aId;
		this.aName = aName;
		this.standard = standard;
		this.amodel = amodel;
		this.accuracy = accuracy;
		this.contractor = contractor;
		this.user = user;
		this.remark = remark;
	}

	public int getaId() {
		return aId;
	}

	public void setaId(int aId) {
		this.aId = aId;
	}

	public String getaName() {
		return aName;
	}

	public void setaName(String aName) {
		this.aName = aName;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getAmodel() {
		return amodel;
	}

	public void setAmodel(String amodel) {
		this.amodel = amodel;
	}

	public double getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(double accuracy) {
		this.accuracy = accuracy;
	}

	public String getContractor() {
		return contractor;
	}

	public void setContractor(String contractor) {
		this.contractor = contractor;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "Appliance [aId=" + aId + ", aName=" + aName + ", standard="
				+ standard + ", amodel=" + amodel + ", accuracy=" + accuracy
				+ ", contractor=" + contractor + ", user=" + user + ", remark="
				+ remark + "]";
	}

}
