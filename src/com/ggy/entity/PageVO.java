package com.ggy.entity;

import java.util.List;

/**分页实体对象，封装分页所需的数据，并提供泛型支持
 * 封装一个每页的实体对象，除了数据之外，还包含其他对象
 * 可以传任何类型的
 * @author Administrator
 *
 */
public class PageVO<T> {
	private int iEcho;
	private int iDisplayLength;//当前显示的每页的长度
	private int iDisplayStart;
	private int iTotalRecords;//总数，即所有的数量
	private int iTotalDisplayRecords;//展示的所有的数量
	private List<T> aaData; // 每页要展示的数据集合

	public PageVO() {
		super();
	}
	//这个两个属性是可以传过来的
	public PageVO(int iDisplayStart, int iDisplayLength) {
		super();
		this.iDisplayStart = iDisplayStart;
		this.iDisplayLength = iDisplayLength;
	}

	public int getiEcho() {
		return iEcho;
	}

	public void setiEcho(int iEcho) {
		this.iEcho = iEcho;
	}

	public int getiDisplayLength() {
		return iDisplayLength;
	}

	public void setiDisplayLength(int iDisplayLength) {
		this.iDisplayLength = iDisplayLength;
	}

	public int getiDisplayStart() {
		return iDisplayStart;
	}

	public void setiDisplayStart(int iDisplayStart) {
		this.iDisplayStart = iDisplayStart;
	}

	public int getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(int iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
		//同时把总数也等于展示的总数量
		this.iTotalDisplayRecords = iTotalRecords;
	}

	public int getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public List<T> getAaData() {
		return aaData;
	}

	public void setAaData(List<T> aaData) {
		this.aaData = aaData;
	}

}
