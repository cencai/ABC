package com.ggy.util;

import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 * 处理Excel文件的工具类
 * @author Administrator
 *
 */
public class ExcelUtil {
	/**
	 * 读取导入的文件
	 * @param is
	 */
	public static void importExcel(InputStream is) {
		try {
			Workbook wb = WorkbookFactory.create(is);
			// 遍历工作薄
			for(int i=0; i<wb.getNumberOfSheets(); i++) {
				Sheet sheet = wb.getSheetAt(i);
				if(sheet == null) {
					return;
				}
				// 遍历工作薄中的每一行
				for(int j=0; j<sheet.getLastRowNum(); j++) {
					Row row = sheet.getRow(j);
					if(row == null) {
						return;
					}
					// 读取每个单元格
					for(int k=0; k<row.getLastCellNum(); k++) {
						Cell cell = row.getCell(k);
						if(cell == null) {
							return;
						}
						// 获取单元格信息（输出或封装对象与数据库交互）
						System.out.println(getValue(cell));
					}
				}
			}
		} catch (EncryptedDocumentException e) {
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static String getValue(Cell cell) {
		// 判断单元格格式，返回内容
		switch(cell.getCellTypeEnum()) {
		case BOOLEAN :
			return cell.getBooleanCellValue() + "";
//			break; // return 不用加break
		case STRING :
			return cell.getStringCellValue();
		case NUMERIC :
			// 有可能是普通数字，或是日期
			if(HSSFDateUtil.isCellDateFormatted(cell)) {
				return  DateUtil.getJavaDate(cell.getNumericCellValue()).toString(); // 转为日期类型
			} else {
				return cell.getNumericCellValue() + "";
			}
		default :
			return cell.getStringCellValue();
		}
	}
	
}
