package com.ggy.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * JDBC工具类
 * 1. 获取数据库连接
 * 2. 查询、更新封装为通用的方法
 * 3. 关闭数据库对象
 * @author Administrator
 *
 */
public class JDBCUtil {
	
	// 静态常量
//	private static final String DB_URL = "jdbc:mysql://localhost:3306/mydb";
//	private static final String DB_USERNAME = "root";
//	private static final String DB_PASSWORD = "root";
//	private static final String DB_DRIVER = "com.mysql.jdbc.Driver";
	// 通过属性文件获取
	private static String DB_URL;
	private static String DB_USERNAME;
	private static String DB_PASSWORD;
	private static String DB_DRIVER;
	
	// 静态块，加载驱动
	static {
		try {
			Properties prop = new Properties();
			prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("jdbc.properties"));
			DB_DRIVER = prop.getProperty("classname");
			DB_URL = prop.getProperty("url");
			DB_USERNAME = prop.getProperty("username");
			DB_PASSWORD = prop.getProperty("userpwd");
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	/**
	 * 获取连接对象的方法
	 * @return
	 */
	private static Connection getConnection() {
		try {
			return DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
		} catch (SQLException e) {
			System.out.println("连接数据库失败。。。");
			return null;
		}
	}
	
	/**
	 * 释放(关闭)数据库对象
	 * @param conn 连接对象
	 * @param stmt 语句对象(预处理语句对象)
	 * @param rs 结果集对象
	 */
	public static void free(Connection conn, Statement stmt, ResultSet rs) {
		try {
			if(rs != null)
				rs.close();
			if(stmt != null)
				stmt.close();
			if(conn != null)
				conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("关闭SQL对象异常。。。");
		}
	}
	
	/**
	 * 查询的工具方法
	 * @param sql 要执行的SQL语句
	 * @param args 要设置的参数
	 * @return 结果集
	 */
	public static ResultSet doQuery(String sql, Object... args) { // 动态参数(不定个数参数)
//	public static ResultSet doQuery(String sql, Object[] args) { // 数组
		Connection conn = getConnection();
		// 先判断是否连接数据库成功
		if(conn == null) {
			return null;
		}
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			// 获取语句对象
			pstmt = conn.prepareStatement(sql);
			// 有参数，则设置参数
			if(args != null && args.length > 0) {
				for(int i=0; i<args.length; i++) {
					//args[i]---有可能是空
					if(args[i] != null)
						pstmt.setObject(i+1, args[i]);
				}
			}
			// 执行语句，处理结果集
			rs = pstmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("执行SQL出错。。。");
		} // 先不能关闭连接，结果集还没处理
		return rs;
	}
	
	/**
	 * 更新的工具方法
	 * @param sql 要执行的SQL语句
	 * @param args 要设置的参数
	 * @return 受影响行数
	 */
	public static int doUpdate(String sql, Object... args) { // 动态参数(不定个数参数)
//	public static int doUpdate(String sql, Object[] args) { // 数组
		Connection conn = getConnection();
		int result = 0;
		// 先判断是否连接数据库成功
		if(conn == null) {
			return result;
		}
		PreparedStatement pstmt = null;
		try {
			// 获取语句对象，并设置参数
			pstmt = conn.prepareStatement(sql);
			// 有参数，则设置参数
			if(args != null && args.length > 0) {
				for(int i=0; i<args.length; i++) {
					if(args[i] != null)
						pstmt.setObject(i+1, args[i]);
				}
			}
			// 执行语句，处理结果集
			result = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("执行SQL出错。。。");
		} finally {
			free(conn, pstmt, null);
		}
		return result;
	}

}
